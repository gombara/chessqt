#ifndef cMAIN_WINDOW_H
#define cMAIN_WINDOW_H

#include <QtGui/QMainWindow>
#include <QtGui/QHBoxLayout>
#include <QtGui/QPushButton>


class cGameBoard;
//!  cMainWindow is the owner of the game screens
/*!
* cMainWindow manages high level interactions between game screens
* it keeps a single instance of cGameBoard and provides it to screens
* Connection of UI elements are also implemeted here
*/
class cMainWindow : public QMainWindow
{
   Q_OBJECT

      public:
   cMainWindow(QWidget *parent = 0, Qt::WFlags flags = 0);
   ~cMainWindow();

   QWidget* loadUiFile(const QString& fName);
   bool loadForms();
   private slots:
   void startSlot();
   void stopSlot();
   void loadSlot();
   void saveSlot();
   void exitSlot();
   void previuosMoveSlot();
   void nextMoveSlot();
   void statusBarText(const QString text);
 signals:
   void exitApplication();
 private:
   void createSignalsSlots();
   void showScreen(unsigned index);
   QWidget* findGameBoard();
   bool insertGameBoardToScreen(unsigned screenIndex);
   bool releaseGameBoard();
   bool loadGame();
   bool saveGame();

   QWidget* centralWidget_;
   QWidget* screens_[3];// for convenience
   QHBoxLayout* centralWLayout_;
   cGameBoard* gameBoard_;// for interactive play
};

#endif // cMAIN_WINDOW_H
