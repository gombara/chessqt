/*
 * GlobalDefinitions.h
 *
 *  Created on: Jun 21, 2015
 *      Author: mecahi
 */

#ifndef SRC_GLOBALDEFINITIONS_H_
#define SRC_GLOBALDEFINITIONS_H_

#define GRID_CELL_COUNT 8
#define GRID_CELL_SIZE 50

enum class PIECE_TYPE{WHITE_PAWN, BLACK_PAWN, KING, QUEEN, ROOK, BISHOP, KNIGHT};
enum class PLAYER{WHITE, BLACK};

#endif /* SRC_GLOBALDEFINITIONS_H_ */
