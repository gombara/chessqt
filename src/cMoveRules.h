#ifndef C_PIECE_RULES
#define C_PIECE_RULES

#include "GlobalDefinitions.h"
#include <QPoint>
//!  cMoveRules is a simple stateless class
/*!
 * It determines if a piece can move to a destination cell.
*/
class cMoveRules
{
public:
   static bool canMove(const PIECE_TYPE& pieceType, 
                const QPoint& prevLoc, 
                const QPoint& nextPoint,
                bool isCellEmpty);
};
#endif
