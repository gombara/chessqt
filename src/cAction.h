#ifndef C_GAME_MOVE_H
#define C_GAME_MOVES_H
#include "GlobalDefinitions.h"
#include <QPoint>
#include <ostream>
#include <QTextStream>

//!  cAction class represent a single move in a game.
/*!
  It is used primarily in save/load game functionality. It represents a move (source, destination, capture etc.)
*/
class cAction
{
public:
   cAction(const PLAYER& player,
         const QPoint& prevCoord,
         const QPoint& nextCoord, int pieceId, int capturedPieceId = -1);
   cAction(){}
   const PLAYER& getPlayer() const { return player_; }
   const QPoint& getPrevCoord() const {return prevCoord_; }
   const QPoint& getNextCoord() const {return nextCoord_; }
   int getPieceId() const { return pieceId_; }
   int getCapturedPieceId() const { return capturedPieceId_; }
private:
   PLAYER player_;
   QString playerInStr_;
   QPoint prevCoord_;
   QPoint nextCoord_;
   int pieceId_;
   int capturedPieceId_;// id of the piece captured by moved  piece (pieceId). -1 if none captured
};

QTextStream& operator<<(QTextStream &out, const cAction& gmove);


#endif
