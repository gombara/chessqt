/*
 * cPieceDrawer.cpp
 *
 *  Created on: Jun 20, 2015
 *      Author: mecahi
 */

#include "cChessPiece.h"

#include <iostream>
#include <QMouseEvent>
#include "GlobalDefinitions.h"
#include <stdexcept>
cChessPiece::cChessPiece(QWidget *parent,
                           const QString& imageFile, QPoint startCell,
                           const PIECE_TYPE& type, const PLAYER& player)
:QLabel(parent),
 size_(GRID_CELL_SIZE),
 type_(type),
 prevGridCoord_(startCell),
 player_(player),
 pieceId_(-1)
{
   if(size_ == 0)
      size_ = 1;
   //   imageLabel = new QLabel;
   this->setBackgroundRole(QPalette::Base);
   this->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
   this->setScaledContents(true);

   connect(this,
           SIGNAL(justMoved(QPoint, cChessPiece*)), parent, SLOT(pieceMoved(QPoint, cChessPiece*)));
   if (!imageFile.isEmpty()) {
      QImage image(imageFile);
      if (image.isNull()) {
         throw std::runtime_error(std::string("image file for the chess piece could not be opened"));
      }
      this->setPixmap(QPixmap::fromImage(image));

   }
   this->resize(GRID_CELL_SIZE, GRID_CELL_SIZE);
   putInToCell(startCell.x(), startCell.y());
   playable_ = (player_ == PLAYER::WHITE);
   this->setFrameShape(QFrame::Box);
   this->setLineWidth(0);
   markMe(playable_);
   this->show();
}

cChessPiece::~cChessPiece()
{

}

void cChessPiece::mousePressEvent(QMouseEvent *ev)
{
   if(!playable_)
      return;
   offset = ev->pos();
   prevGridCoord_ = getGridCellCoord();
}

void cChessPiece::mouseReleaseEvent(QMouseEvent *ev)
{
   if(!playable_)
         return;
   /// snap piece to the center of the grid cell when the mouse is released
   QPoint lo = getGridCellCoord();
   this->move(lo.x() * size_, lo.y() * size_);
   emit justMoved(getGridCellCoord(), this);
}

void cChessPiece::mouseMoveEvent(QMouseEvent *ev)
{
   if(!playable_)
         return;
   if(ev->buttons() & Qt::LeftButton)
   {
      this->move(mapToParent(ev->pos() - offset));
   }
}

QPoint cChessPiece::getGridCellCoord() const
{
   /// compute the middle point of the label
   int x = this->rect().topLeft().x() + (this->size().width()) / 2;
   int y = this->rect().topLeft().y() + (this->size().height()) / 2;
   QPoint lo(mapToParent(QPoint(x,y)));
   unsigned px, py;

   if(lo.x() < 0)
      px = 0;
   else
      px = lo.x();

   if(lo.y() < 0)
      py = 0;
   else
      py = lo.y();

   QPoint point(px / size_, py / size_);

   if(point.x() >= GRID_CELL_COUNT)
      point.setX(GRID_CELL_COUNT - 1);

   if(point.y() >= GRID_CELL_COUNT)
      point.setY(GRID_CELL_COUNT - 1);

   return point;
}

void cChessPiece::putInToCell(const QPoint point)
{
   putInToCell(point.x(), point.y());
}

void cChessPiece::putInToCell(unsigned cx, unsigned cy)
{
   if(cx >= GRID_CELL_COUNT)
      cx = GRID_CELL_COUNT;
   if(cy >= GRID_CELL_COUNT)
        cy = GRID_CELL_COUNT;
   this->move(cx * GRID_CELL_SIZE, cy * GRID_CELL_SIZE);
}

const PIECE_TYPE cChessPiece::getType() const
{
   return type_;
}

const QPoint& cChessPiece::getPrevCoord() const
{
   return  prevGridCoord_;
}

bool cChessPiece::getPlayable() const
{
   return playable_;
}

void cChessPiece::setPlayable(bool flag)
{
   if(playable_ == flag)
      return;
   playable_ = flag;
   markMe(playable_);
}

const PLAYER& cChessPiece::getPlayer() const
{
   return player_;
}

void cChessPiece::markMe(bool flag)
{
   if(flag)
   {
      this->setStyleSheet("background-color: rgba(255, 215, 215, 100);");
   }
   else
   {
      this->setStyleSheet("background-color: rgba(255, 255, 255, 0);");
   }
}

void cChessPiece::setPieceId(int id)
{
  pieceId_ = id;
}

int cChessPiece::getPieceId() const
{
   return pieceId_;
}
