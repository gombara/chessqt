#ifndef cPIECE_DRAWER_H
#define cPIECE_DRAWER_H

#include <QtGui/QLabel>
#include "GlobalDefinitions.h"

//!  cChessPiece class represent an instance of a chess piece.
/*!
 * it stores several state parameters (like location, owning player) it also handles drawing the piece (thru QLabel)
*/
class cChessPiece : public QLabel
{
   Q_OBJECT

public:
   cChessPiece(QWidget *parent, 
                const QString& imageFile, QPoint startCell,
                const PIECE_TYPE& type, const PLAYER& player);
   ~cChessPiece();
//   void paintEvent(QPaintEvent * event );
   void mouseMoveEvent(QMouseEvent *ev);
   void mousePressEvent(QMouseEvent *ev);
   void mouseReleaseEvent(QMouseEvent *ev);
   void putInToCell(unsigned cx, unsigned cy);
   void putInToCell(const QPoint point);
   const PIECE_TYPE getType() const;
   const QPoint& getPrevCoord() const;
   QPoint getGridCellCoord() const;
   bool getPlayable() const;
   void setPlayable(bool flag);
   const PLAYER& getPlayer() const;
   void setPieceId(int id);
   int getPieceId() const;
signals:
   void justMoved(QPoint pos, cChessPiece* piece);
private:
   void markMe(bool flag);
   bool playable_;
   PLAYER player_;
   QPoint prevGridCoord_;
   QPoint offset;
   unsigned size_;
   PIECE_TYPE type_;
   int pieceId_;
};


#endif
