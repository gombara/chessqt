#include <QtGui/QApplication>
#include "cMainWindow.h"
#include <stdexcept>
#include <iostream>

int main(int argc, char *argv[])
{
   QApplication app(argc, argv);
   try
   {
      cMainWindow mainWindow;
      app.connect(&mainWindow, SIGNAL(exitApplication()), &app, SLOT(quit()));
      mainWindow.move(-300, -200);
      mainWindow.show();
      return app.exec();
   }
   catch(std::runtime_error& e)
   {
      std::cerr << e.what() << std::endl;
      app.exit();
   }
   return 0;
}
