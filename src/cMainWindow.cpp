#include "cMainWindow.h"
#include <iostream>
#include <QtUiTools/QtUiTools>
#include <QFileDialog>
#include <QStatusBar>
#include "cGameBoard.h"
#include <stdexcept>
cMainWindow::cMainWindow(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags),
	  centralWidget_(nullptr),
	  screens_{nullptr, nullptr, nullptr},
	  centralWLayout_(nullptr)
{
   centralWidget_ = new QWidget(this);
   setCentralWidget(centralWidget_);
   centralWLayout_ = new QHBoxLayout(centralWidget_);
   statusBarText("");
   
   if(!loadForms())
   {
      //bail out. qt object will handle their own destruction
      throw std::runtime_error(std::string("could not load all the forms"));
   }
   else
   {
      centralWLayout_->addWidget(screens_[0]);
      centralWLayout_->addWidget(screens_[1]);
      centralWLayout_->addWidget(screens_[2]);

      gameBoard_ = new cGameBoard(nullptr);
      gameBoard_->setObjectName("GameBoard");
      connect(gameBoard_,
                  SIGNAL(statusTextSignal(const QString)), this, SLOT(statusBarText(const QString)));

      if(!insertGameBoardToScreen(0))
         throw std::runtime_error(std::string("could not add game board to screen1"));
      showScreen(0);
      createSignalsSlots();
   }

}



QWidget* cMainWindow::loadUiFile(const QString& fName)
{
    QUiLoader loader;

    QFile file(fName);

    if(!file.open(QFile::ReadOnly))
    {
       return nullptr;
    }

    QWidget *formWidget = loader.load(&file, this);
    formWidget->hide();
    file.close();

    return formWidget;
}

bool cMainWindow::loadForms()
{
   screens_[0] = loadUiFile(":/forms/screen1.ui");
   screens_[1] = loadUiFile(":/forms/screen2.ui");
   screens_[2] = loadUiFile(":/forms/screen3.ui");
   for(int i = 0; i < 3; ++i)
   {
      if(!screens_[i])
         return false;
   }
   return true;
}

void cMainWindow::createSignalsSlots()
{
   // screen1
   QPushButton* startButton = screens_[0]->findChild<QPushButton*>("startButton");
   if(startButton)
   {
      connect(startButton,
            SIGNAL(pressed()), this, SLOT(startSlot()));
   }
   QPushButton* loadButton = screens_[0]->findChild<QPushButton*>("loadButton");
   if(loadButton)
   {
      connect(loadButton,
            SIGNAL(pressed()), this, SLOT(loadSlot()));
   }
   QPushButton* exitButton = screens_[0]->findChild<QPushButton*>("exitButton");
   if(exitButton)
   {
      connect(exitButton,
            SIGNAL(pressed()), this, SLOT(exitSlot()));
   }
   // screen2
   QPushButton* stopButton = screens_[1]->findChild<QPushButton*>("stopButton");
   if(stopButton)
   {
      connect(stopButton,
            SIGNAL(pressed()), this, SLOT(stopSlot()));
   }
   QPushButton* saveButton = screens_[1]->findChild<QPushButton*>("saveButton");
   if(saveButton)
   {
      connect(saveButton,
            SIGNAL(pressed()), this, SLOT(saveSlot()));
   }
   // screen3
   QPushButton* startButton2 = screens_[2]->findChild<QPushButton*>("startButton");
   if(startButton2 )
   {
      connect(startButton2 ,
            SIGNAL(pressed()), this, SLOT(startSlot()));
   }
   QPushButton* prevButton = screens_[2]->findChild<QPushButton*>("prevButton");
   if(prevButton )
   {
      connect(prevButton ,
            SIGNAL(pressed()), this, SLOT(previuosMoveSlot()));
   }
   QPushButton* nextButton = screens_[2]->findChild<QPushButton*>("nextButton");
   if(nextButton )
   {
      connect(nextButton ,
            SIGNAL(pressed()), this, SLOT(nextMoveSlot()));
   }

   QPushButton* loadButton2 = screens_[2]->findChild<QPushButton*>("loadButton");
   if(loadButton2 )
   {
      connect(loadButton2 ,
            SIGNAL(pressed()), this, SLOT(loadSlot()));
   }
}

void cMainWindow::exitSlot()
{
   emit exitApplication();
}

void cMainWindow::startSlot()
{
   if(!insertGameBoardToScreen(1))
      return;
   gameBoard_->startNewGame();
   showScreen(1);
}

void cMainWindow::saveSlot()
{
   saveGame();
}

void cMainWindow::loadSlot()
{
   if(!loadGame())
   {
      statusBarText("Error in loading");
      return;
   }
   if(!insertGameBoardToScreen(2))
      return;
   gameBoard_->startLoadedGame();
   showScreen(2);
}

void cMainWindow::stopSlot()
{
   if(!insertGameBoardToScreen(0))
         return;
   gameBoard_->endGame();
//   for(int i = 0; i < 2; ++i)
//         players_[i]->endGame(*playGameBoard_);
   showScreen(0);
}

void cMainWindow::previuosMoveSlot()
{
   if(gameBoard_)
      gameBoard_->replayPreviousMove();
}

void cMainWindow::nextMoveSlot()
{
   if(gameBoard_)
         gameBoard_->replayNextMove();
}

void cMainWindow::statusBarText(const QString text)
{
   statusBar()->showMessage(text);
}

cMainWindow::~cMainWindow()
{
}

void cMainWindow::showScreen(unsigned index)
{
   /// hide all of them
   for(unsigned i = 0; i < 3; ++i)
      if(screens_[i])
         screens_[i]->hide();
   //show screens_[index]
   if(screens_[index])
      screens_[index]->show();
}

/// find game board in screens
QWidget* cMainWindow::findGameBoard()
{
   for(unsigned i = 0; i < 3; ++i)
   {
      QFrame* boardFrame = screens_[i]->findChild<QFrame*>("boardFrame");
      QHBoxLayout* boardFrameLayout= screens_[i]->findChild<QHBoxLayout*>("boardFrameLayout");
      if(!boardFrame || ! boardFrameLayout)
         continue;
      cGameBoard* board = screens_[i]->findChild<cGameBoard*>("GameBoard");
      if(board)
         return screens_[i];
   }
   return nullptr;
}

/// remove the game board from the screen it is currently attached
bool cMainWindow::releaseGameBoard()
{

   for(unsigned i = 0; i < 3; ++i)
      {
         QFrame* boardFrame = screens_[i]->findChild<QFrame*>("boardFrame");
         QHBoxLayout* boardFrameLayout= screens_[i]->findChild<QHBoxLayout*>("boardFrameLayout");
         if(!boardFrame || ! boardFrameLayout)
            continue;
         cGameBoard* board = screens_[i]->findChild<cGameBoard*>("GameBoard");
         if(board)
         {
            boardFrameLayout->removeWidget(board);
            return true;
         }
      }
   return false;
}

bool cMainWindow::insertGameBoardToScreen(unsigned screenIndex)
{
   if(screenIndex >= 3)
      return false;
   if(!screens_[screenIndex])
      return false;
   if(!gameBoard_)
      return false;

   releaseGameBoard();
   QFrame* boardFrame = screens_[screenIndex]->findChild<QFrame*>("boardFrame");
   QHBoxLayout* boardFrameLayout= screens_[screenIndex]->findChild<QHBoxLayout*>("boardFrameLayout");
   if(!boardFrame || ! boardFrameLayout)
      return false;
   boardFrameLayout->addWidget(gameBoard_);
   return true;
}

bool cMainWindow::loadGame()
{
   if(!gameBoard_)
      return false;
   QString fileName = QFileDialog::getOpenFileName(this, "Load Game", "./", tr("Text files (*.txt)"));
   if(fileName.isEmpty())
      return false;
   return gameBoard_->loadGame(fileName);
}

bool cMainWindow::saveGame()
{
   if(!gameBoard_)
      return false;
   QString fileName = QFileDialog::getSaveFileName(this, "Save Game", "./", tr("Text files (*.txt)"));
   if(fileName.isEmpty())
      return false;
   
   return gameBoard_->saveGame(fileName);
}
