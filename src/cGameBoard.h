/*
 * cGameBoard.h
 *
 *  Created on: Jun 19, 2015
 *      Author: mecahi
 */

#ifndef CGAMEBOARD_H_
#define CGAMEBOARD_H_


#include <QtGui/QFrame>
#include <QMap>
#include <QVector>

#include "cAction.h"
#include "cChessPiece.h"
#include "cMoveRules.h"
//!  cGameBoard class implements the game logic and board drawing
/*!
 * cGameBoard is the main driver class. it manages all the game pieces and provides
 * game logic both for interactive and loaded game.
*/
class cGameBoard : public QFrame
{
   Q_OBJECT

public:
   cGameBoard(QWidget *parent = 0);
   ~cGameBoard();
   void paintEvent(QPaintEvent * event );
   void mouseMoveEvent(QMouseEvent *ev);
   void startNewGame();
   void endGame();
   bool saveGame(const QString& fname);
   bool loadGame(const QString& fname);
   void startLoadedGame();
   bool replayNextMove();
   bool replayPreviousMove();
   void cleanBoard();
signals:
   void statusTextSignal(const QString);
public slots:
   void pieceMoved(QPoint, cChessPiece*);
private:
   cGameBoard(){}
   cGameBoard(const cGameBoard&){}
   void placePieces();
   void switchTurnPlayer();
   void deletePieces();
   void endTurn(const cAction& move);
   bool isCoordValid(QPoint& coord);
   int capturePiece(cChessPiece* piece);
   int capturePiece(int id);
   int unCapturePiece(int id);
   QVector<cChessPiece*> lookIntoCell(const QPoint& cellCoord);// return pointer to the piece in cell if cell is occupied
                                                               //we mave have more than one piece at one time
   QVector<cChessPiece*> lookIntoCell(const QPoint& cellCoord, cChessPiece* piece);// this variant populates the vector with the pieces
                                                                                                // residing in the cell that are different from @piece
   bool replayMove(const cAction& move, bool backward = false);
   inline unsigned getFlatIndex(const QPoint& point)
   {return 8 * point.y() + point.x();}

   inline QPoint getGridIndex(unsigned flatIndex)
   {return QPoint(flatIndex % 8, flatIndex / 8);}
   /// called when @p pieces moved into a cell. @piecesInCell is potentially list of pieces in that cell
   int capture(cChessPiece* piece, QVector<cChessPiece*> piecesInCell);

   QMap<unsigned, cChessPiece*> pieces_;
   QVector<cAction> moves_;
   unsigned cellw_;
   unsigned cellh_;
   cMoveRules rules_;
   PLAYER turnPlayer_;

   bool replaying_;// true if a loaded game is replayed
   int currentReplayIndex_;
};

#endif /* SRC_CGAMEBOARD_H_ */
