#include "cMoveRules.h"

#include <QtGlobal>
#include <iostream>

bool cMoveRules::canMove(const PIECE_TYPE& pieceType, 
      const QPoint& prev,
      const QPoint& next,
      bool isCellEmpty)
{
   if(next.x() >= GRID_CELL_COUNT)
      return false;
   if(next.y() >= GRID_CELL_COUNT)
      return false;
   if(next.x() < 0)
      return false;
   if(next.y() < 0)
      return false;

   if(next.y() == prev.y() && next.x() == prev.x())
      return false;

   switch(pieceType)
   {
   case PIECE_TYPE::WHITE_PAWN:
   {
      bool diagonal = (qAbs(next.x() - prev.x()) == 1 && prev.y() - next.y() == 1);
      bool forward = (prev.x() == next.x() && prev.y() - next.y() <= 2 && prev.y() > next.y());
      if(!isCellEmpty && forward)
         return false;
      if(isCellEmpty && forward)
         return true;
      if(isCellEmpty && diagonal)
         return false;
      if(!isCellEmpty && diagonal)
         return true;
      return false;
      break;
   }
   case PIECE_TYPE::BLACK_PAWN:
   {
      bool forward = (prev.x() == next.x() && next.y() - prev.y() <= 2 && prev.y() < next.y());
      bool diagonal = (!isCellEmpty && qAbs(next.x() - prev.x()) == 1 && next.y() - prev.y() == 1);

      if(!isCellEmpty && forward)
         return false;
      if(isCellEmpty && forward)
         return true;
      if(isCellEmpty && diagonal)
         return false;
      if(!isCellEmpty && diagonal)
         return true;
      return false;
      break;

      break;
   }
   case PIECE_TYPE::BISHOP:
      if(qAbs(next.x() - prev.x()) == qAbs(next.y() - prev.y()))
         return true;
      return false;
      break;
   case PIECE_TYPE::KING:
      if(qAbs(next.x() - prev.x()) == 1)
         return true;
      if(qAbs(next.y() - prev.y()) == 1)
         return true;
      return false;
      break;
   case PIECE_TYPE::QUEEN:
      if(next.x() == prev.x())
         return true;
      if(next.y() == prev.y())
         return true;
      if(qAbs(next.x() - prev.x()) ==
            qAbs(next.y() - prev.y()))
         return true;
      return false;
      break;
   case PIECE_TYPE::ROOK:
      if(next.x() == prev.x())
         return true;
      if(next.y() == prev.y())
         return true;
      return false;
      break;
   case PIECE_TYPE::KNIGHT:
      if((qAbs(next.x() - prev.x()) == 1) &&
            (qAbs(next.y() - prev.y()) == 2))
         return true;
      if((qAbs(next.y() - prev.y()) == 1) &&
            (qAbs(next.x() - prev.x()) == 2))
         return true;
      return false;
      break;
   default:
      break;
   }

}


