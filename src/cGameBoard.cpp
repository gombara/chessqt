#include "cGameBoard.h"
#include <iostream>
#include <QPainter>
#include <QtSvg/QSvgRenderer>
#include <QFile>
#include <QTextStream>

cGameBoard::cGameBoard(QWidget *parent)
:QFrame(parent),
 cellw_(50),
 cellh_(50),
 turnPlayer_(PLAYER::WHITE),
 replaying_(false),
 currentReplayIndex_(0)
{
   setMinimumSize(8 * cellw_ + 1, 8 * cellh_ + 1);
   //    cPieceDrawer* newPiece = new cPieceDrawer(this,
   //          "/home/mecahi/gombara/chessQt/resources/sBishop.png", cellw_);
}

cGameBoard::~cGameBoard()
{
}

void cGameBoard::startNewGame()
{
   replaying_ = false;
   turnPlayer_ = PLAYER::WHITE;
   statusTextSignal("Playing: White's Turn");
   try
   {
      placePieces();
   }
   catch(std::exception& e)
   {
      std::cerr << e.what() << std::endl;
   }
}

void cGameBoard::startLoadedGame()
{
   replaying_ = true;
   currentReplayIndex_ = 0;
   try
   {
      placePieces();
   }
   catch(std::exception& e)
   {
      std::cerr << e.what() << std::endl;
   }
   /// make pieces non-playable
   for(QMap<unsigned, cChessPiece*>::iterator iter = pieces_.begin();
         iter != pieces_.end(); ++iter)
   {
      if(iter.value())
         iter.value()->setPlayable(false);
   }
}

bool cGameBoard::replayNextMove()
{
   if(currentReplayIndex_ >= moves_.size() ||
         currentReplayIndex_ < 0)
      return false;
   replayMove(moves_[currentReplayIndex_]);
   emit statusTextSignal("Replayed move " + QString::number(currentReplayIndex_ + 1)
         + " of " + QString::number(moves_.size()));
   ++currentReplayIndex_;
   return true;
}

bool cGameBoard::replayPreviousMove()
{

   if(currentReplayIndex_ > moves_.size() ||
            currentReplayIndex_ <= 0)
      return false;
   emit statusTextSignal("Replayed move " + QString::number(currentReplayIndex_ + 1)
            + " of " + QString::number(moves_.size()));
   --currentReplayIndex_;
   replayMove(moves_[currentReplayIndex_], true);
   return true;
}

bool cGameBoard::replayMove(const cAction& move, bool backward /*=false*/)
{
   //find the piece in the map
   int pieceId = move.getPieceId();
   QMap<unsigned, cChessPiece*>::iterator it =  pieces_.find(pieceId);

   if(it == pieces_.end())
      return false;

   QPoint destionationCell;

   if(backward)
      destionationCell = move.getPrevCoord();
   else
      destionationCell = move.getNextCoord();

   it.value()->putInToCell(destionationCell);

   QVector<cChessPiece*> piecesInDestination = lookIntoCell(destionationCell, it.value());
   if(!backward)
      capturePiece(move.getCapturedPieceId());
   else
      unCapturePiece(move.getCapturedPieceId());

   return true;
}

void cGameBoard::cleanBoard()
{
   deletePieces();
   moves_.clear();
}

void cGameBoard::endGame()
{
   statusTextSignal("");
   cleanBoard();
}

void cGameBoard::deletePieces()
{
   for(QMap<unsigned, cChessPiece*>::iterator iter = pieces_.begin();
         iter != pieces_.end(); ++iter)
      delete iter.value();
   pieces_.clear();
}

void cGameBoard::placePieces()
{
   deletePieces();
   int pieceId = 0;
   pieces_.insert(pieceId++,
         new cChessPiece(this, ":/images/sBishop.png", QPoint(2,0), PIECE_TYPE::BISHOP, PLAYER::BLACK));
   pieces_.insert(pieceId++,
         new cChessPiece(this, ":/images/sBishop.png", QPoint(5,0), PIECE_TYPE::BISHOP, PLAYER::BLACK));
   pieces_.insert(pieceId++,
         new cChessPiece(this, ":/images/sKnight.png", QPoint(1,0), PIECE_TYPE::KNIGHT, PLAYER::BLACK));
   pieces_.insert(pieceId++,
         new cChessPiece(this, ":/images/sKnight.png", QPoint(6,0), PIECE_TYPE::KNIGHT, PLAYER::BLACK));
   pieces_.insert(pieceId++,
         new cChessPiece(this, ":/images/sRook.png", QPoint(7,0), PIECE_TYPE::ROOK, PLAYER::BLACK));

   pieces_.insert(pieceId++,
         new cChessPiece(this, ":/images/sRook.png", QPoint(0,0), PIECE_TYPE::ROOK, PLAYER::BLACK));
   pieces_.insert(pieceId++,
         new cChessPiece(this, ":/images/sKing.png", QPoint(4,0), PIECE_TYPE::KING, PLAYER::BLACK));
   pieces_.insert(pieceId++,
         new cChessPiece(this, ":/images/sQueen.png", QPoint(3,0), PIECE_TYPE::QUEEN, PLAYER::BLACK));
   for(int i = 0; i < 8; ++i)
      pieces_.insert(pieceId++,
            new cChessPiece(this, ":/images/sPawn.png", QPoint(i,1), PIECE_TYPE::BLACK_PAWN, PLAYER::BLACK));

   pieces_.insert(pieceId++,
         new cChessPiece(this, ":/images/wBishop.png", QPoint(2,7), PIECE_TYPE::BISHOP, PLAYER::WHITE));
   pieces_.insert(pieceId++,
         new cChessPiece(this, ":/images/wBishop.png", QPoint(5,7), PIECE_TYPE::BISHOP, PLAYER::WHITE));
   pieces_.insert(pieceId++,
         new cChessPiece(this, ":/images/wKnight.png", QPoint(1,7), PIECE_TYPE::KNIGHT, PLAYER::WHITE));
   pieces_.insert(pieceId++,
         new cChessPiece(this, ":/images/wKnight.png", QPoint(6,7), PIECE_TYPE::KNIGHT, PLAYER::WHITE));
   pieces_.insert(pieceId++,
         new cChessPiece(this, ":/images/wRook.png", QPoint(7,7), PIECE_TYPE::ROOK, PLAYER::WHITE));
   pieces_.insert(pieceId++,
         new cChessPiece(this, ":/images/wRook.png", QPoint(0,7), PIECE_TYPE::ROOK, PLAYER::WHITE));
   pieces_.insert(pieceId++,
         new cChessPiece(this, ":/images/wKing.png", QPoint(4,7), PIECE_TYPE::KING, PLAYER::WHITE));
   pieces_.insert(pieceId++,
         new cChessPiece(this, ":/images/wQueen.png", QPoint(3,7), PIECE_TYPE::QUEEN, PLAYER::WHITE));
   for(int i = 0; i < 8; ++i)
      pieces_.insert(pieceId++,
            new cChessPiece(this, ":/images/wPawn.png", QPoint(i,6), PIECE_TYPE::WHITE_PAWN, PLAYER::WHITE));
   for(QMap<unsigned, cChessPiece*>::iterator iter = pieces_.begin();
            iter != pieces_.end(); ++iter)
      iter.value()->setPieceId(iter.key());
}

void cGameBoard::paintEvent(QPaintEvent * event )
{

   unsigned pox = 0;
   unsigned poy = 0;
   int isRed = -1;

   QBrush redBrush(QColor(182, 182, 182), Qt::SolidPattern);
   QBrush greenBrush(QColor(102, 102, 102), Qt::SolidPattern);
   QPainter painter(this);
   for(unsigned i = 0; i < 8; ++i)
   {
      if(i%2 == 0)
         isRed = 1;
      else
         isRed = -1;
      for(unsigned j = 0; j < 8; j++)
      {
         QRect rectangle(pox + i * cellw_, poy + j * cellh_, cellw_, cellh_);
         painter.drawRect(rectangle);
         isRed *= -1;
         if(isRed == -1)
            painter.fillRect(rectangle, redBrush);
         else
            painter.fillRect(rectangle, greenBrush);
      }
   }
}

void cGameBoard::mouseMoveEvent(QMouseEvent *ev)
{
}

void cGameBoard::pieceMoved(QPoint nextPoint, cChessPiece* piece)
{
   if(!piece)
      return;
   /// check if other pieces reside in the cell
   QVector<cChessPiece*> piecesInDestination = lookIntoCell(nextPoint, piece);

   bool canMove = rules_.canMove(piece->getType(),
         piece->getPrevCoord(),
         piece->getGridCellCoord(),
         piecesInDestination.size() == 0);

   if(!canMove)
   {
      piece->putInToCell(piece->getPrevCoord());
      return;
   }
   int capturedPieceId = capture(piece, piecesInDestination);
   if(!replaying_)
   {
      try{
         endTurn(cAction(turnPlayer_, piece->getPrevCoord(), nextPoint, piece->getPieceId(), capturedPieceId));         
      }
      catch(std::logic_error& err)
      {
         std::cerr << err.what() << std::endl;
      }
   }
}

int cGameBoard::capture(cChessPiece* piece, QVector<cChessPiece*> piecesInCell)
{
   for(int i = 0; i < piecesInCell.size(); ++i)
   {
      if(piecesInCell[i] != piece)
      {
         return capturePiece(piecesInCell[i]);
      }
   }
   return -1;
}

void cGameBoard::switchTurnPlayer()
{
   if(turnPlayer_ == PLAYER::WHITE)
   {
      turnPlayer_ = PLAYER::BLACK;
      statusTextSignal("Playing: Black's Turn");
   }
   else
   {
      turnPlayer_ = PLAYER::WHITE;
      statusTextSignal("Playing: White's Turn");
   }
}

void cGameBoard::endTurn(const cAction& move)
{
   /// add the move to moves list
   moves_.push_back(move);
   switchTurnPlayer();

   for(QMap<unsigned, cChessPiece*>::iterator iter = pieces_.begin();
         iter != pieces_.end(); ++iter)
   {
      if(iter.value())
         iter.value()->setPlayable(turnPlayer_ == iter.value()->getPlayer());
   }
}

bool cGameBoard::saveGame(const QString& fname)
{
   if(fname.isEmpty())
      return false;
   QFile historyFile(fname);
   if(!historyFile.open(QIODevice::WriteOnly|QIODevice::Truncate))
      return false;
   QTextStream out(&historyFile);
   for(std::size_t i = 0; i < moves_.size(); ++i)
   {
      out << moves_[i];
   }
   historyFile.close();
   emit statusTextSignal("Game Saved to " + fname);
   return true;
}



bool cGameBoard::loadGame(const QString& fname)
{
   cleanBoard();
   if(fname.isEmpty())
      return false;
   QFile historyFile(fname);
   if(!historyFile.open(QIODevice::ReadOnly))
      return false;

   QTextStream in(&historyFile);
   while(!in.atEnd()) {
      QString line = in.readLine();    
      QStringList slist = line.split(", ");
      PLAYER tPlayer = PLAYER::WHITE;
      if(slist.size() != 7)
         return false;
      if(slist[0].indexOf("black", 0, Qt::CaseInsensitive) != -1)
         tPlayer = PLAYER::BLACK;
      QPoint pre(slist[1].toInt(), slist[2].toInt());
      QPoint next(slist[3].toInt(), slist[4].toInt());
      if(isCoordValid(pre) || isCoordValid(next))
         return false;
      try{
         moves_.push_back(cAction(tPlayer, pre, next, slist[5].toInt(), slist[6].toInt() ));
      }
      catch(std::logic_error& err)
      {
         std::cerr << err.what() << std::endl;
      }

   }
   historyFile.close();
   emit statusTextSignal("Game Loaded from " + fname);
   return true;
}

bool cGameBoard::isCoordValid(QPoint& coord)
{
   if(coord.x() < 0)
      return false;
   if(coord.y() < 0)
      return false;
   if(coord.x() >= GRID_CELL_COUNT)
      return false;
   if(coord.y() >= GRID_CELL_COUNT)
      return false;
}

QVector<cChessPiece*> cGameBoard::lookIntoCell(const QPoint& cellCoord)
{
   return lookIntoCell(cellCoord, nullptr);
}

QVector<cChessPiece*> cGameBoard::lookIntoCell(const QPoint& cellCoord, cChessPiece* piece)
{
   QVector<cChessPiece*> pieces;
      for(QMap<unsigned, cChessPiece*>::iterator iter = pieces_.begin();
               iter != pieces_.end(); ++iter)
      {
         if(iter.value() && iter.value()->getGridCellCoord() == cellCoord)
         {
            if(!piece)
               pieces.push_back(iter.value());
            else
            {
               if(piece != iter.value())
                  pieces.push_back(iter.value());
            }
         }
      }
      return pieces;
}

int cGameBoard::capturePiece(cChessPiece* piece)
{
   int pieceId = piece->getPieceId();
   return capturePiece(pieceId);
}

int cGameBoard::capturePiece(int pieceId)
{
   if(pieceId == -1)
      return -1;
   QMap<unsigned, cChessPiece*>::iterator iter = pieces_.find(pieceId);
   if(iter != pieces_.end())
   {
      if(iter.value())
      {
         iter.value()->hide();
         return pieceId;
      }
   }
   return -1;
}

int cGameBoard::unCapturePiece(int pieceId)
{
   if(pieceId == -1)
      return -1;
   QMap<unsigned, cChessPiece*>::iterator iter = pieces_.find(pieceId);
   if(iter != pieces_.end())
   {
      if(iter.value())
      {
         iter.value()->show();
         return pieceId;
      }
   }
   return -1;
}
