#include "cAction.h"
#include <stdexcept>
#include <iostream>
cAction::cAction(const PLAYER& player,
         const QPoint& prevCoord,
         const QPoint& nextCoord, int pieceId,
         int capturedPieceId)
   :player_(player),
    prevCoord_(prevCoord),
    nextCoord_(nextCoord),
    pieceId_(pieceId),
    capturedPieceId_(capturedPieceId)
{
   if(prevCoord_.x() < 0 || prevCoord_.x() >= GRID_CELL_COUNT ||
      prevCoord_.y() < 0 || prevCoord_.y() >= GRID_CELL_COUNT ||
      nextCoord_.x() < 0 || nextCoord_.x() >= GRID_CELL_COUNT ||
      nextCoord_.y() < 0 || nextCoord_.y() >= GRID_CELL_COUNT)
      throw std::logic_error(std::string("invalid grid coordinate in action construction"));

   playerInStr_ = "White";
   if(player_ == PLAYER::BLACK)
      playerInStr_ = "Black";
}


QTextStream& operator<<(QTextStream  &out, const cAction& gmove)
{
   QString player("Black, ");
   if(gmove.getPlayer() == PLAYER::WHITE)
      player = "White, ";
   out <<  player << gmove.getPrevCoord().x() << ", " << gmove.getPrevCoord().y() <<
      ", " << gmove.getNextCoord().x() << ", " << gmove.getNextCoord().y() << ", " <<
      gmove.getPieceId() << ", " << gmove.getCapturedPieceId() << "\n";
   return out;
}
