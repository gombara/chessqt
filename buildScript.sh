#!/bin/bash

ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BUILD_DIR="$ROOT_DIR/build"
INSTALL_DIR="$ROOT_DIR/install"
SRC_DIR="$ROOT_DIR/src"
BUILD_TYPE="Release"



####################################################################
####################################################################
function print_usage
{
    echo "$0: [[options]]"
    echo ""
    echo "  Available options are:"
    echo "   -d"
    echo "      Builds a debug version."
    echo "   -r"
    echo "      Builds a release version (default)."
    echo "   -c <gcc path>"
    echo "   -q <add qt path to cmake find library path>"
    echo "      Adds the qt path so that find_xxx functionality of cmake searches under that path"
    echo "   -h"
    echo "      Print this help text. ... in the hope to be helpful."
}
####################################################################
RELEASE_BUILD=true
while getopts ":q:c:rdh" OPT; do
    case $OPT in
        d) RELEASE_BUILD=false;;
        r) RELEASE_BUILD=false;;
        q) PREFIX_PATH="$OPTARG";;
        c) GCC_DIR="$OPTARG";;
        h) print_usage; exit 1;;
    esac
done

echo $ROOT_DIR

if [ ! -d "$BUILD_DIR" ]; then
    mkdir $BUILD_DIR
fi


cd $BUILD_DIR


BUILD_TYPE_STRING="-DCMAKE_BUILD_TYPE=Debug"

if [ "$RELEASE_BUILD" = true ] ; then
    BUILD_TYPE_STRING="-DCMAKE_BUILD_TYPE=Release"
fi

CMAKE_STRING="-DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} -DCMAKE_VERBOSE_MAKEFILE=TRUE $BUILD_TYPE_STRING"

# if [[ ! -z  $OSG_DIR  ]]; then
#     PREFIX_PATH="$PREFIX_PATH;$OSG_DIR"
# fi

# if [[ ! -z  $QT_DIR  ]]; then
#     PREFIX_PATH="$PREFIX_PATH;$QT_DIR"
# fi

if [[ ! -z  $GCC_DIR  ]]; then
    CMAKE_STRING="$CMAKE_STRING -DGCC_PATH=$GCC_DIR"
fi

if [[ ! -z  $PREFIX_PATH  ]]; then
    CMAKE_STRING="$CMAKE_STRING -DCMAKE_PREFIX_PATH=$PREFIX_PATH"
fi


cmake ${SRC_DIR} ${CMAKE_STRING}

cmake --build . --target install -- "-j8"

